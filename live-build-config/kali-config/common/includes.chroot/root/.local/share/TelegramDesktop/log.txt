[2021.09.03 09:40:35] Launched version: 3000001, install beta: [FALSE], alpha: 0, debug mode: [FALSE]
[2021.09.03 09:40:35] Executable dir: /root/Telegram/, name: Telegram
[2021.09.03 09:40:35] Initial working dir: /root/Telegram/
[2021.09.03 09:40:35] Working dir: /root/.local/share/TelegramDesktop/
[2021.09.03 09:40:35] Command line: ./Telegram
[2021.09.03 09:40:35] Executable path before check: /root/Telegram/Telegram
[2021.09.03 09:40:35] Logs started
[2021.09.03 09:40:35] Launcher filename: appimagekit_9ee185519cb92781dfa8e1de8d5222c7-Telegram_Desktop.desktop
[2021.09.03 09:40:35] Connecting local socket to /run/user/0/c8a98f2fe164925688f81a251c978b91-{87A94AB0-E370-4cde-98D3-ACC110C5967D}...
[2021.09.03 09:40:35] This is the only instance of Telegram, starting server and app...
[2021.09.03 09:40:35] Moved logging from '/root/.local/share/TelegramDesktop/log_start0.txt' to '/root/.local/share/TelegramDesktop/log.txt'!
[2021.09.03 09:40:35] Primary screen DPI: 96.0757
[2021.09.03 09:40:35] Font: from ':/gui/fonts/DAOpenSansRegular.ttf' loaded 'DAOpenSansRegular'
[2021.09.03 09:40:35] Font: from ':/gui/fonts/DAVazirRegular.ttf' loaded 'DAVazirRegular'
[2021.09.03 09:40:35] Font: from ':/gui/fonts/DAOpenSansRegularItalic.ttf' loaded 'DAOpenSansRegularItalic'
[2021.09.03 09:40:35] Font: from ':/gui/fonts/DAVazirRegular.ttf' loaded 'DAVazirRegular'
[2021.09.03 09:40:35] Font: from ':/gui/fonts/DAOpenSansSemiboldAsBold.ttf' loaded 'DAOpenSansSemibold'
[2021.09.03 09:40:35] Font: from ':/gui/fonts/DAVazirMediumAsBold.ttf' loaded 'DAVazirMedium'
[2021.09.03 09:40:35] Font: from ':/gui/fonts/DAOpenSansSemiboldItalicAsBold.ttf' loaded 'DAOpenSansSemiboldItalic'
[2021.09.03 09:40:35] Font: from ':/gui/fonts/DAVazirMediumAsBold.ttf' loaded 'DAVazirMedium'
[2021.09.03 09:40:35] Font: from ':/gui/fonts/DAOpenSansSemiboldAsBold.ttf' loaded 'DAOpenSansSemibold'
[2021.09.03 09:40:35] Font: from ':/gui/fonts/DAVazirMediumAsBold.ttf' loaded 'DAVazirMedium'
[2021.09.03 09:40:35] Font: from ':/gui/fonts/DAOpenSansSemiboldItalicAsBold.ttf' loaded 'DAOpenSansSemiboldItalic'
[2021.09.03 09:40:35] Font: from ':/gui/fonts/DAVazirMediumAsBold.ttf' loaded 'DAVazirMedium'
[2021.09.03 09:40:35] Icon theme: Flat-Remix-Blue-Dark
[2021.09.03 09:40:35] Fallback icon theme: breeze
[2021.09.03 09:40:37] OpenAL Logging Level: (not set)
[2021.09.03 09:40:37] Audio Playback Devices: Built-in Audio Analog Stereo
[2021.09.03 09:40:37] Audio Playback Default Device: Built-in Audio Analog Stereo
[2021.09.03 09:40:37] Audio Capture Devices: Built-in Audio Analog Stereo;Monitor of Built-in Audio Analog Stereo
[2021.09.03 09:40:37] Audio Capture Default Device: Built-in Audio Analog Stereo
[2021.09.03 09:40:37] Not using D-Bus global menu.
[2021.09.03 09:40:37] Using Unity launcher counter.
[2021.09.03 09:40:37] System tray available: [TRUE]
[2021.09.03 09:40:37] OpenGL Profile: Compatibility.
[2021.09.03 09:40:37] OpenGL Renderer: NV92
[2021.09.03 09:40:37] OpenGL Vendor: nouveau
[2021.09.03 09:40:37] OpenGL Version: 3.3 (Compatibility Profile) Mesa 20.3.4
[2021.09.03 09:40:37] OpenGL Extensions: GL_ARB_explicit_attrib_location, GL_ARB_uniform_buffer_object, GL_ARB_internalformat_query2, GL_EXT_draw_range_elements, GL_S3_s3tc, GL_ATI_blend_equation_separate, GL_EXT_point_parameters, GL_OES_read_format, GL_NV_vdpau_interop, GL_ARB_vertex_attrib_binding, GL_EXT_texture_shared_exponent, GL_EXT_depth_bounds_test, GL_NV_texture_rectangle, GL_ARB_map_buffer_alignment, GL_MESA_texture_signed_rgba, GL_ANGLE_texture_compression_dxt3, GL_EXT_direct_state_access, GL_ARB_texture_env_add, GL_ARB_vertex_array_bgra, GL_ARB_multi_bind, GL_ARB_shader_objects, GL_ARB_get_texture_sub_image, GL_ARB_texture_storage, GL_ARB_stencil_texturing, GL_ARB_shading_language_packing, GL_EXT_texture_array, GL_ARB_enhanced_layouts, GL_ARB_clear_texture, GL_MESA_window_pos, GL_ARB_pixel_buffer_object, GL_ARB_framebuffer_object, GL_EXT_gpu_program_parameters, GL_ARB_depth_texture, GL_ARB_fragment_coord_conventions, GL_EXT_provoking_vertex, GL_EXT_polygon_offset_clamp, GL_EXT_stencil_two_side, GL_ARB_shading_language_100, GL_EXT_framebuffer_sRGB, GL_ATI_separate_stencil, GL_ARB_shader_clock, GL_ARB_texture_env_combine, GL_ARB_parallel_shader_compile, GL_ARB_copy_image, GL_ARB_draw_elements_base_vertex, GL_ARB_shading_language_420pack, GL_EXT_texture_env_add, GL_EXT_packed_depth_stencil, GL_ARB_draw_instanced, GL_ARB_vertex_type_2_10_10_10_rev, GL_ARB_texture_env_dot3, GL_ARB_texture_query_levels, GL_EXT_subtexture, GL_NV_fog_distance, GL_ARB_copy_buffer, GL_EXT_framebuffer_object, GL_EXT_texture_rectangle, GL_EXT_vertex_array, GL_EXT_rescale_normal, GL_ARB_program_interface_query, GL_ARB_compatibility, GL_ARB_texture_float, GL_ARB_texture_mirrored_repeat, GL_SGIS_texture_lod, GL_EXT_blend_equation_separate, GL_ANGLE_texture_compression_dxt5, GL_ARB_shader_texture_image_samples, GL_INGR_blend_func_separate, GL_NV_texture_barrier, GL_EXT_timer_query, GL_ARB_occlusion_query2, GL_ARB_viewport_array, GL_ARB_instanced_arrays, GL_ARB_sync, GL_KHR_parallel_shader_compile, GL_EXT_texture_filter_anisotropic, GL_ARB_fragment_layer_viewport, GL_EXT_secondary_color, GL_ARB_shader_subroutine, GL_EXT_texture_compression_latc, GL_EXT_draw_buffers2, GL_NV_conditional_render, GL_ARB_provoking_vertex, GL_ARB_conditional_render_inverted, GL_ATI_texture_float, GL_ARB_shadow, GL_ARB_vertex_program, GL_EXT_framebuffer_multisample_blit_scaled, GL_AMD_conservative_depth, GL_ARB_texture_stencil8, GL_EXT_window_rectangles, GL_ARB_texture_view, GL_ARB_color_buffer_float, GL_ARB_clear_buffer_object, GL_EXT_texture_sRGB, GL_EXT_EGL_sync, GL_ARB_texture_border_clamp, GL_MESA_pack_invert, GL_MESA_shader_integer_functions, GL_NV_packed_depth_stencil, GL_ARB_texture_env_crossbar, GL_EXT_abgr, GL_EXT_blend_func_separate, GL_ARB_half_float_pixel, GL_ARB_texture_compression, GL_OES_EGL_image, GL_ARB_texture_compression_rgtc, GL_EXT_texture_buffer_object, GL_EXT_texture_edge_clamp, GL_IBM_rasterpos_clip, GL_KHR_context_flush_control, GL_EXT_compiled_vertex_array, GL_EXT_stencil_wrap, GL_AMD_performance_monitor, GL_ARB_pipeline_statistics_query, GL_EXT_blend_minmax, GL_ARB_texture_cube_map, GL_NV_texgen_reflection, GL_ARB_get_program_binary, GL_EXT_texture_object, GL_KHR_texture_compression_astc_ldr, GL_ATI_draw_buffers, GL_NV_half_float, GL_ARB_direct_state_access, GL_KHR_no_error, GL_ARB_debug_output, GL_ARB_texture_multisample, GL_EXT_texture_cube_map, GL_ARB_vertex_array_object, GL_SGIS_texture_border_clamp, GL_ARB_multitexture, GL_ARB_draw_buffers, GL_ARB_texture_barrier, GL_ARB_vertex_shader, GL_ARB_cull_distance, GL_ARB_conservative_depth, GL_EXT_texture_compression_s3tc, GL_ARB_shader_bit_encoding, GL_ARB_explicit_uniform_location, GL_ARB_point_sprite, GL_ARB_polygon_offset_clamp, GL_ARB_vertex_type_10f_11f_11f_rev, GL_IBM_multimode_draw_arrays, GL_EXT_multi_draw_arrays, GL_ARB_multisample, GL_ARB_texture_rectangle, GL_ARB_invalidate_subdata, GL_EXT_texture_swizzle, GL_IBM_texture_mirrored_repeat, GL_ARB_base_instance, GL_EXT_fog_coord, GL_NV_blend_square, GL_EXT_texture_lod_bias, GL_EXT_texture_sRGB_decode, GL_EXT_texture_compression_dxt1, GL_ARB_sampler_objects, GL_KHR_debug, GL_ARB_ES2_compatibility, GL_EXT_draw_instanced, GL_ARB_texture_rg, GL_NV_primitive_restart, GL_ARB_fragment_program, GL_EXT_packed_float, GL_ARB_compressed_texture_pixel_storage, GL_ARB_blend_func_extended, GL_EXT_separate_specular_color, GL_EXT_blend_subtract, GL_ARB_ES3_compatibility, GL_NV_light_max_exponent, GL_SGIS_texture_edge_clamp, GL_ARB_timer_query, GL_SUN_multi_draw_arrays, GL_ARB_transpose_matrix, GL_NV_texture_env_combine4, GL_ARB_vertex_buffer_object, GL_EXT_shader_integer_mix, GL_ARB_shader_texture_lod, GL_ARB_texture_buffer_range, GL_ARB_shading_language_include, GL_EXT_packed_pixels, GL_EXT_texture_snorm, GL_ARB_fragment_program_shadow, GL_ARB_clip_control, GL_ARB_depth_buffer_float, GL_EXT_texture_mirror_clamp, GL_EXT_framebuffer_multisample, GL_EXT_gpu_shader4, GL_ATI_fragment_shader, GL_ATI_texture_compression_3dc, GL_ARB_texture_mirror_clamp_to_edge, GL_ATI_texture_env_combine3, GL_SGIS_generate_mipmap, GL_ARB_texture_rgb10_a2ui, GL_ARB_texture_buffer_object, GL_EXT_texture_env_dot3, GL_EXT_vertex_array_bgra, GL_ARB_point_parameters, GL_EXT_blend_color, GL_EXT_pixel_buffer_object, GL_ARB_internalformat_query, GL_ARB_seamless_cube_map, GL_ARB_framebuffer_sRGB, GL_EXT_shadow_funcs, GL_EXT_texture3D, GL_EXT_texture_integer, GL_ATI_texture_mirror_once, GL_ARB_buffer_storage, GL_AMD_shader_trinary_minmax, GL_NV_copy_image, GL_EXT_EGL_image_storage, GL_ARB_depth_clamp, GL_ARB_window_pos, GL_ARB_derivative_control, GL_ARB_texture_swizzle, GL_EXT_copy_texture, GL_ARB_fragment_shader, GL_ARB_map_buffer_range, GL_EXT_framebuffer_blit, GL_EXT_texture_compression_rgtc, GL_APPLE_packed_pixels, GL_ARB_texture_storage_multisample, GL_NV_depth_clamp, GL_ARB_occlusion_query, GL_ARB_texture_non_power_of_two, GL_ARB_half_float_vertex, GL_ARB_robustness, GL_EXT_texture_env_combine, GL_KHR_texture_compression_astc_sliced_3d, GL_EXT_texture, GL_EXT_bgra, GL_EXT_transform_feedback, GL_ARB_arrays_of_arrays, GL_ARB_texture_filter_anisotropic, GL_ARB_separate_shader_objects
[2021.09.03 09:40:37] OpenGL: QOpenGLContext created, version: 3.3.
[2021.09.03 09:40:37] OpenGL: [TRUE] (OverlayWidget)
[2021.09.03 09:40:37] Using SNI tray icon.
[2021.09.03 09:40:37] Update Info: MTP is unavailable.
[2021.09.03 09:40:38] Notification daemon product name: Plasma
[2021.09.03 09:40:38] Notification daemon vendor name: KDE
[2021.09.03 09:40:38] Notification daemon version: 5.20.5
[2021.09.03 09:40:38] Notification daemon specification version: 1.2
[2021.09.03 09:40:38] Notification daemon capabilities: body, body-hyperlinks, body-markup, body-images, icon-static, actions, inline-reply, x-kde-urls, x-kde-origin-name, x-kde-display-appname, inhibitions
[2021.09.03 09:46:11] RPC Error: request 15 got fail with code 400, error PHONE_CODE_INVALID
[2021.09.03 09:47:09] RPC Error: request 17 got fail with code 400, error PHONE_NUMBER_INVALID
[2021.09.03 09:54:34] RPC Error: request 24 got fail with code 400, error PHONE_NUMBER_BANNED
[2021.09.03 10:05:06] RPC Error: request 28 got fail with code 400, error PHONE_CODE_INVALID
